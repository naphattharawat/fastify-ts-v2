import { Knex } from 'knex';

import { TUser } from '../../types/user';

export class User {
    // Properties
    userId!: string;
    username!: string;
    password!: string;
    fname!: string;
    lname!: string;

    private db!: Knex;

    /**
     * Set Database connection
     * @param db Knex
     */
    setConnection(db: Knex) {
        this.db = db;
    }

    /**
     * Register new user
     * @param db Knex
     * @returns
     */
    save() {
        return this.db('users').insert({
            username: this.username,
            password: this.password,
            fname: this.fname,
            lname: this.lname,
        });
    }
    /**
     * Update user
     * @param db Knex
     * @returns
     */
    update() {
        return this.db('users').where('user_id', this.userId).update({
            fname: this.fname,
            lname: this.lname,
        });
    }

    /**
     * Remove user
     * @returns
     */
    remove() {
        return this.db('users').where('user_id', this.userId).del();
    }

    /**
     * Get user list
     * @param limit
     * @param offset
     * @returns
     */
    list(limit: number, offset: number): Promise<TUser[]> {
        return this.db('users')
            .select('user_id', 'username', 'fname', 'lname')
            .orderBy('fname', 'asc')
            .limit(limit)
            .offset(offset);
    }

    /**
     * Get total number of users
     * @returns
     */
    async total(): Promise<number> {
        const result = await this.db('users').count('* as total').first();
        return Number(result?.total || '0');
    }
}
