import { Knex } from 'knex';

import { TFileInfo, TUser } from '../../types/user';

export class FileModel {
    private db!: Knex;

    // File properties
    fileId!: string;
    userId!: string;
    fileName!: string;
    type!: string;
    path!: string;

    /**
     * Set Database connection
     * @param db Knex
     */
    setConnection(db: Knex) {
        this.db = db;
    }

    /**
     * User login
     * @param username
     */
    save(): Promise<TUser> {
        return this.db('files').insert({
            file_name: this.fileName,
            file_path: this.path,
            file_type: this.type,
            user_id: this.userId,
        });
    }

    remove(): Promise<any> {
        return this.db('files').where('file_id', this.fileId).del();
    }

    getInfo(): Promise<TFileInfo> {
        return this.db('files').where('file_id', this.fileId).first();
    }
}
