import { compare, hash } from 'bcrypt';

export class Hash {
    /**
     * Hash password
     * @param password
     * @returns Hashed
     */
    hashPassword(password: string): Promise<string> {
        const saltRounds = 12;
        return hash(password, saltRounds);
    }

    /**
     * Compare password
     * @param password Password
     * @param hash Password hashed
     * @returns Boolean
     */
    comparePassword(password: string, hash: string): Promise<boolean> {
        return compare(password, hash);
    }
}
