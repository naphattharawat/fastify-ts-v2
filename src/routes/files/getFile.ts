import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { Knex } from 'knex';
import fs from 'node:fs';
import path from 'node:path';

import { FileModel } from '../../models/file';

export default async (fastify: FastifyInstance): Promise<void> => {
    const db = fastify.db as Knex;
    const fileModel = new FileModel();
    // Set database connection
    fileModel.setConnection(db);
    // Set upload path
    const UPLOAD_PATH = process.env.UPLOAD_PATH || './uploads';

    fastify.get('/:fileId', async function (request: FastifyRequest, reply: FastifyReply) {
        const { fileId } = request.params as { fileId: string };
        try {
            fileModel.fileId = fileId;
            const fileInfo = await fileModel.getInfo();

            if (!fileInfo) {
                return reply.status(404).send({
                    ok: false,
                    statusCode: 40400,
                    error: 'File not found.',
                    reqId: request.id,
                });
            }

            const filePath = path.join(UPLOAD_PATH, fileInfo.file_path);
            // Check file exists
            if (!fs.existsSync(filePath)) {
                return reply.status(404).send({
                    ok: false,
                    statusCode: 40401,
                    error: 'File not exists.',
                    reqId: request.id,
                });
            }

            // Send file to client
            const fileData = fs.readFileSync(filePath);
            const file = fileInfo.file_name;

            return reply
                .headers({
                    'Content-Type': 'application/octet-stream',
                    'Content-Disposition': `attachment; filename=${file}`,
                })
                .send(fileData);
        } catch (error) {
            request.log.error({
                message: JSON.stringify(error),
                reqId: request.id,
                module: 'FILE',
            });

            return reply.status(500).send({
                ok: false,
                statusCode: 10500,
                error: 'Internal server error.',
                reqId: request.id,
            });
        }
    });
};
