import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { Knex } from 'knex';
import fs from 'node:fs';
import path from 'node:path';

import { FileModel } from '../../models/file';

export default async (fastify: FastifyInstance): Promise<void> => {
    const db = fastify.db as Knex;
    const fileModel = new FileModel();
    // Set database connection
    fileModel.setConnection(db);
    // Set upload path
    const UPLOAD_PATH = process.env.UPLOAD_PATH || './uploads';

    fastify.delete<{ Params: { fileId: string } }>(
        '/:fileId',
        async function (request: FastifyRequest, reply: FastifyReply) {
            const { fileId } = request.params as { fileId: string };
            try {
                fileModel.fileId = fileId;
                const fileInfo = await fileModel.getInfo();

                // File in database not found
                if (!fileInfo) {
                    return reply.status(404).send({
                        ok: false,
                        statusCode: 40400,
                        error: 'File not found.',
                        reqId: request.id,
                    });
                }

                // Check file exists
                const filePath = path.join(UPLOAD_PATH, fileInfo.file_path);

                if (!fs.existsSync(filePath)) {
                    return reply.status(404).send({
                        ok: false,
                        statusCode: 40401,
                        error: 'File not exists.',
                        reqId: request.id,
                    });
                }

                // Remove file from database
                await fileModel.remove();
                // remove file from storage
                fs.rmSync(filePath, { force: true });
                // Logging
                request.log.warn({
                    message: 'File removed.',
                    reqId: request.id,
                    module: 'FILE',
                    data: {
                        file_id: fileId,
                    },
                });

                return reply.send({ ok: true, statusCode: 40200, reqId: request.id });
            } catch (error) {
                // Set logging
                request.log.error({
                    message: JSON.stringify(error),
                    reqId: request.id,
                    module: 'FILE',
                });

                return reply.status(500).send({
                    ok: false,
                    statusCode: 10500,
                    error: 'Internal server error.',
                    reqId: request.id,
                });
            }
        }
    );
};
