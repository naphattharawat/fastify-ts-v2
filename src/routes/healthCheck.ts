import { FastifyInstance } from 'fastify';

export default async (fastify: FastifyInstance): Promise<void> => {
    fastify.get('/health', async function () {
        return { ok: true };
    });
};
