/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Please remove this file in production use
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
import { FastifyInstance } from 'fastify';

import { Hash } from '../utils/hash';

export default async (fastify: FastifyInstance): Promise<void> => {
    const hashModel = new Hash();

    fastify.get('/demo/jwt/sign', async function (_request, reply) {
        const token = fastify.jwt.sign({ sub: '1234567890', name: 'John Doe' });
        return reply.send({ token });
    });

    fastify.get(
        '/demo/jwt/verify',
        {
            onRequest: [fastify.authenticate],
        },
        async function (request, reply) {
            const decoded = request.jwtDecoded;
            return reply.send(decoded);
        }
    );

    fastify.get('/demo/hash', async function (request, reply) {
        const { password: plaintextPassword = '123456' } = request.query as { password: string };
        const hash = await hashModel.hashPassword(plaintextPassword);
        return reply.send({ hash, passwprd: plaintextPassword });
    });
};
