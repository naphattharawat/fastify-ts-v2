import fp from 'fastify-plugin';

import helmet from '@fastify/helmet';

export default fp(async (fastify) => {
    fastify.register(helmet, {
        contentSecurityPolicy: {
            useDefaults: false,
            directives: {
                'default-src': ['self'],
            },
        },
        xXssProtection: true,
        dnsPrefetchControl: {
            allow: false,
        },
        xPoweredBy: false,
    });
});
